#include <thread>
#include <chrono>
#include "SolutionReplay.hpp"

SolutionReplay::SolutionReplay(std::vector<CubeMove> shuffle, std::vector<CubeMove> solution)
{
    this->shuffle = shuffle;
    this->solution = solution;
}

void SolutionReplay::SetSolution(std::vector<CubeMove> v)
{
    solution = v;
}

void SolutionReplay::SetShuffle(std::vector<CubeMove> v)
{
    shuffle = v;
}

void SolutionReplay::Replay(const int ms_delay)
{
    Cube c;
    c.Rotate(shuffle);

    c.View();
    std::cout << "Energija: " << c.Energy() << "\n";

    std::this_thread::sleep_for(std::chrono::milliseconds(ms_delay));

    for(const auto & move:solution)
    {
        for(int i = 0; i < 11; i++)
        std::cout << "\x1b[2K\x1b[A";
        c.Rotate(move);
        c.View();
        std::cout << "Energija: " << c.Energy() << "\n";
        std::this_thread::sleep_for(std::chrono::milliseconds(ms_delay));
    }
}
