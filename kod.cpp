#include "Cube.hpp"
#include "MonteCarloSolver.hpp"
#include "mpi.h"
#include <chrono>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include "SolutionReplay.hpp"

using namespace std::literals::chrono_literals;

int main(int argc, char **argv)
{
    int rank, size, in;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    std::vector<CubeMove> shuffle;
    Cube kocka;

    std::vector<MPI_Request> requests;

    if (rank == 0)
    {
        std::cout << "1  --down clockwise\n";
        std::cout << "2  --down anti clockwise\n";
        std::cout << "3  --up clockwise\n";
        std::cout << "4  --up anti clockwise\n";
        std::cout << "5  --front clockwise\n";
        std::cout << "6  --front anti clockwise\n";
        std::cout << "7  --back clockwise\n";
        std::cout << "8  --back anti clockwise\n";
        std::cout << "9  --right clockwise\n";
        std::cout << "10 --right anti clockwise\n";
        std::cout << "11 --left clockwise\n";
        std::cout << "12 --left anti clockwise\n";

        std::cout << "\nUnesite rotacije odvojenim razmacima (0 za izlaz): " << std::endl;
        while (std::cin >> in)
        {
            if (in > 0 && in < 13)
                shuffle.push_back((CubeMove)(in - 1));
            else
            {
                break;
            }
        }

        int array_size = shuffle.size();
        for (int i = 1; i < size; i++)
        {
            MPI_Send(&array_size, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
            MPI_Send(shuffle.data(), array_size, MPI_INT, i, 1, MPI_COMM_WORLD);
        }

        kocka.Rotate(shuffle);
        kocka.View();
        std::cout << "Pocetna energija: " << kocka.Energy() << "\n\n";

        std::vector<int> counts;
        counts.resize(size - 1);

        MPI_Request req;
        for (int i = 1; i < size; i++)
        {
            MPI_Irecv(counts.data() + (i - 1), 1, MPI_INT, i, 3, MPI_COMM_WORLD, &req);
            requests.push_back(req);
        }
        std::vector<CubeMove> solution;

        int got_solution = 0;
        MPI_Status status;
        int index = 1, index_rez = 0, dot = 0;
        while (!got_solution)
        {
            index = 0;
            for (auto request : requests)
            {
                MPI_Test(&request, &got_solution, &status);
                // std::cout << "Provjeravam " << index + 1 << ", rijesio: " << got_solution << "\n";
                if (!index_rez && got_solution)
                {
                    solution.resize(counts[index]);
                    // std::cout << "Dosao do ovdje -1\n";
                    // std::cout << "\n" << counts[index] << ", " << counts[index_rez] << ", " << index_rez << "\n";

                    MPI_Recv(solution.data(), counts[index], MPI_INT, index + 1, 4, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                    index_rez = index;
                    break;
                    // int j = 0;
                    // for (auto &request : requests)
                    //     if (j++ != index)
                    //         MPI_Cancel(&request);
                }
                index++;
            }
            if (!index_rez)
            {
                std::cout << "\x1b[2KRjesavam";
                for (int i = 0; i < dot % 4; i++)
                {
                    std::cout << ".";
                }
                std::cout << "\r" << std::flush;
                dot++;
            }

            std::this_thread::sleep_for(1000ms);
        }
        std::cout << "\x1b[2K\r";

        std::vector<MPI_Request> reqs;
        int msg1 = 0, msg2 = 0;
        reqs.resize(size - 1);
        for (int i = 1; i < size; i++)
        {
            MPI_Isend(&msg1, 1, MPI_INT, i, 8, MPI_COMM_WORLD, reqs.data() + i - 1);
            MPI_Send(&msg2, 1, MPI_INT, i, 9, MPI_COMM_WORLD);
        }

        for (auto &req : reqs)
        {
            MPI_Status status;
            int done = 0;
            MPI_Test(&req, &done, &status);
            if (!done)
                MPI_Cancel(&req);
        }

        std::cout << std::endl;

        std::cout << "Rjesenje (proces - " << index_rez + 1 << "): \n";
        for (const auto &move : solution)
            std::cout << (int)move + 1 << " ";
        std::cout << "\n\n";

        kocka.Rotate(solution);
        SolutionReplay s {solution, shuffle};
        s.Replay(1000);
    }
    else
    {
        std::ofstream logger(std::to_string(rank) + " - log.txt");
        int count = 0;
        MPI_Recv(&count, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        shuffle.resize(count);
        MPI_Recv(shuffle.data(), count, MPI_INT, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        MonteCarloSolver cube_solver = MonteCarloSolver(shuffle);
        auto solution = cube_solver.Solve();

        count = solution.size();

        logger << "Dobio (" << rank << "): ";
        for (const auto &move : shuffle)
            logger << (int)move + 1 << " ";
        logger << std::endl;

        MPI_Request req1;
        MPI_Request req2;

        if (count)
        {
            MPI_Isend(&count, 1, MPI_INT, 0, 3, MPI_COMM_WORLD, &req1);
            MPI_Isend(solution.data(), count, MPI_INT, 0, 4, MPI_COMM_WORLD, &req2);
        }

        logger << "Poslao: ";
        for (const auto &move : solution)
            logger << (int)move + 1 << " ";
        logger << std::endl;

        MPI_Recv(&count, 1, MPI_INT, 0, 9, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        if (count)
        {
            int done = 0;
            MPI_Status status;
            MPI_Test(&req1, &done, &status);
            if (!done)
                MPI_Cancel(&req1);
            MPI_Test(&req2, &done, &status);
            if (!done)
                MPI_Cancel(&req2);
        }
        logger << "Rank: " << rank << " end\n";
    }

    MPI_Finalize();

    return 0;
}
